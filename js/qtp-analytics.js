var clicked = false;

/**
 * Add a function which executes after timeout if the it is not called.
 */
function createFunctionWithTimeout(callback, opt_timeout) {
  var called = false;
  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, opt_timeout || 1000);
  return fn;
}

/**
 * Track a link or where document location is changable.
 */
function trackLinkClick(id, category, action, label) {
	var elmId = '#' + id,
		elm = $(elmId);

	elm.on('click', function (event) {
		ga('send', {
			hitType: 'event',
			eventCategory: category,
			eventAction: action,
			eventLabel: label,
			hitCallback: createFunctionWithTimeout(function () {
				clicked = true;
				// window.location.href = $(elmId).attr('href');
			})
		});
	});
}

// Track "Get App Button" click actions
$("a[js-data='download']").on('click', function (event) {
  event.preventDefault();
  ga('send', {
  	hitType: 'event',
		eventCategory: 'Get App Button',
		eventAction: 'Clicked',
		eventLabel: 'Get App Button'
  });
});

// Track Android Download link click
trackLinkClick('qtp-android', 'Platform Android', 'Download Android', 'Get Android App');
// Track iOS Download link click
trackLinkClick('qtp-ios', 'Platform iOS', 'Download iOS', 'Get iOS App');