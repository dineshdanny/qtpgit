var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('default', function() {
	console.log('hello Zell');
});

gulp.task('sass', function() {
	return gulp.src('scss/**/*.scss')
		.pipe(sass({
			includePaths: require('node-bourbon').includePaths
		}))
		.pipe(gulp.dest('css'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('watch', ['browserSync', 'sass'], function(){
	gulp.watch('scss/**/*.scss', ['sass']);
	gulp.watch('css/custom.css', browserSync.reload);
	gulp.watch('*.html', browserSync.reload);
	gulp.watch('js/**/*.js', browserSync.reload);
});

gulp.task('browserSync', function(){
	browserSync.init({
		server: {
			baseDir: ''
		}
	})	
});

gulp.task('reload', function(){
	return gulp.src('index.html')
		.pipe(browserSync.reload({
			stream: true
	}))
});